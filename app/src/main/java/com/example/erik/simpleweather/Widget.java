package com.example.erik.simpleweather;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;

import java.util.Arrays;

public class Widget extends AppWidgetProvider  {
    Intent intent;
    PendingIntent pendingIntent;
    Context context;

    private static final String SYNC_CLICKED="WidgetClicked";
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        RemoteViews rv=new RemoteViews(context.getPackageName(),R.layout.widget);
        ComponentName cn=new ComponentName(context,Widget.class);

        rv.setTextViewText(R.id.current_temperature_field_widg, WeatherActivityFragment.currentTemperatureField.getText().toString());
        rv.setTextViewText(R.id.weather_icon_widg, WeatherActivityFragment.detailsField.getText().toString());
        rv.setTextViewText(R.id.city_field_widg, WeatherActivityFragment.cityField.getText().toString());
        rv.setOnClickPendingIntent(R.id.rl, getPendingSelfIntent(context,SYNC_CLICKED));
        appWidgetManager.updateAppWidget(cn,rv);

    }

    protected PendingIntent getPendingSelfIntent(Context context, String action){
        Intent intent=new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context,0,intent,0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context,intent);

        if(SYNC_CLICKED.equals(intent.getAction())){
            RemoteViews rv=new RemoteViews(context.getPackageName(),R.layout.widget);
            ComponentName cn=new ComponentName(context,Widget.class);
            AppWidgetManager awp=AppWidgetManager.getInstance(context);
            rv.setTextViewText(R.id.current_temperature_field_widg, WeatherActivityFragment.currentTemperatureField.getText().toString());
            rv.setTextViewText(R.id.weather_icon_widg, WeatherActivityFragment.detailsField.getText().toString());
            rv.setTextViewText(R.id.city_field_widg, WeatherActivityFragment.cityField.getText().toString());
            awp.updateAppWidget(cn,rv);
        }
    }



}
